<a href="https://flathub.org/apps/details/org.gnome.design.IconLibrary">
<img src="https://flathub.org/assets/badges/flathub-badge-i-en.png" width="190px" />
</a>

# Icon Library

<img src="https://gitlab.gnome.org/World/design/icon-library/raw/master/data/icons/org.gnome.design.IconLibrary.svg" width="128" height="128" />
<p>Symbolic icons for your apps.</p>

## Screenshots

<div align="center">
![screenshot](data/resources/screenshots/screenshot1.png)
</div>

## Hack on Icon Library
To build the development version of Icon Library and hack on the code
see the [general guide](https://wiki.gnome.org/Newcomers/BuildProject)
for building GNOME apps with Flatpak and GNOME Builder.

You are expected to follow our [Code of Conduct](https://wiki.gnome.org/Foundation/CodeOfConduct) when participating in project
spaces.
