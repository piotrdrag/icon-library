# Simple script to update the icons from https://gitlab.gnome.org/Teams/Design/icon-development-kit
# git is required
import yaml
import os
import shutil
import subprocess
import glob
import json

icon_paths = set()
ABS_PATH = os.path.dirname(os.path.abspath(__file__))
repo_url = "https://gitlab.gnome.org/Teams/Design/icon-development-kit-www.git"

repo_dir = os.path.join(ABS_PATH, "icon-development-kit-www")
icons_dir = os.path.join(repo_dir, "img", "symbolic")
icons_output_dir = os.path.join(ABS_PATH, "data", "resources", "icon-dev-kit")
icons_meta_output = os.path.join(
    ABS_PATH, "data", "resources", "icons_dev_kit.json")

if not os.path.exists(repo_dir):
    subprocess.call(["git", "clone", "--depth", "1", repo_url], cwd=ABS_PATH)


def generate_gresource():
    with open('data/icons.gresource.xml', 'w') as gresource:
        gresource.write('<?xml version="1.0" encoding="UTF-8"?>\n<gresources>\n')
        gresource.write('  <gresource prefix="/org/gnome/design/IconLibrary/icons/16x16/actions/">\n')
        sorted_paths = list(icon_paths)
        sorted_paths.sort()
        for icon in sorted_paths:
            icon_str = '    <file preprocess="xml-stripblanks" alias="{0}">resources/icon-dev-kit/{0}</file>\n'.format(icon)
            gresource.write(icon_str)

        gresource.write("  </gresource>\n</gresources>\n")


# Remove previously downloaded icons
for filename in os.listdir(icons_output_dir):
    file_path = os.path.join(icons_output_dir, filename)
    try:
        if os.path.isfile(file_path) or os.path.islink(file_path):
            os.unlink(file_path)
        elif os.path.isdir(file_path):
            shutil.rmtree(file_path)
    except Exception as e:
        print('Failed to delete %s. Reason: %s' % (file_path, e))

for icon in glob.glob(f"{icons_dir}/**/*-symbolic.svg"):
    output_icon = os.path.join(icons_output_dir, os.path.basename(icon))
    icon_paths.add(os.path.basename(icon))
    if os.path.exists(output_icon):
        os.remove(output_icon)
    shutil.move(icon, output_icon)

generate_gresource()

data_dir = os.path.join(repo_dir, "_data", "icons.yaml")
with open(data_dir, 'r') as handle:
    icons_meta = yaml.load(handle.read(), Loader=yaml.BaseLoader)


meta_output = []

for icon in icons_meta:
    tags = icon["tags"]

    icon_name = icon["filename"]
    if icon_name == "clock":
        # Clock icon produce a weird artifact, ignore it for now
        continue

    if not list(filter(lambda icon: icon["name"] == icon_name, meta_output)):
        context = icon["context"]
        if len(context) >= 3:
            context = context.title()
        else:
            context = context.upper()
        if context == "Noexport-Bits":
            continue

        meta_output.append({
            "name": icon_name,
            "tags": tags,
            "context": context,
        })


meta_output.sort(key=lambda icon: icon["name"].lower())
with open(icons_meta_output, 'w') as handle:
    json.dump(meta_output, handle, indent=2)

shutil.rmtree(repo_dir)
