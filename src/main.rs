use gettextrs::*;
use gtk::{gio, glib};

mod application;
mod config;
mod models;
mod widgets;

use application::Application;
use config::{GETTEXT_PACKAGE, LOCALEDIR};

fn main() -> glib::ExitCode {
    pretty_env_logger::init();

    gtk::init().expect("Failed to initalize gtk");

    glib::set_application_name("Icon Library");
    // Prepare i18n
    setlocale(LocaleCategory::LcAll, "");
    bindtextdomain(GETTEXT_PACKAGE, LOCALEDIR).expect("Could not bind text domain");
    textdomain(GETTEXT_PACKAGE).expect("Could not set text domain");

    let res = gio::Resource::load(config::PKGDATADIR.to_owned() + "/resources.gresource")
        .expect("Could not load resources");
    let dev_icons_res = gio::Resource::load(config::PKGDATADIR.to_owned() + "/icons.gresource")
        .expect("Could not load resources");
    gio::resources_register(&res);
    gio::resources_register(&dev_icons_res);

    Application::run()
}
